"use strict";
const serverless = require("serverless-http");
const express = require("express");
const app = express();
app.use(express.json());

app.post("/getResult", async function (req, res) {
  try {
    const { input } = req.body;

    console.log("input", input);
    let orderedListOfParentAndChildren = [];
    if (input.length > 0) {
      // filter out all first level parents ( no parent id)
      let tempInput = input.filter((item) => item.parent_id);
      orderedListOfParentAndChildren = input.filter((item) => !item.parent_id);

      let currentLevelOfParents = orderedListOfParentAndChildren;
      let nextLevelOfParent = [];

      // for level 2 and beyond, we use while loop till the tempInput is empty
      while (tempInput.length > 0) {
        for (let i = 0; i < currentLevelOfParents.length; i++) {
          const parent = currentLevelOfParents[i];
          console.log("parent 1 ", parent);
          nextLevelOfParent.push(
            ...tempInput.filter((item) => item.parent_id === parent.id)
          );
          tempInput = tempInput.filter((item) => item.parent_id !== parent.id);
        }
        currentLevelOfParents = nextLevelOfParent;
        nextLevelOfParent = [];
        orderedListOfParentAndChildren.push(...currentLevelOfParents);
      }

      // let tempInput2 = input.filter((item) => item.parent_id);
      // while (tempInput2.length > 0) {
      //   orderedListOfParentAndChildren.forEach((parent) => {
      //     console.log("parent 2 ", parent);
      //     orderedListOfParentAndChildren.push(
      //       ...tempInput2.filter((item) => item.parent_id === parent.id)
      //     );
      //     tempInput2 = tempInput2.filter(
      //       (item) => item.parent_id !== parent.id
      //     );
      //   });
      // }

      console.log(
        "orderedListOfParentAndChildren",
        orderedListOfParentAndChildren
      );
    } else {
      throw new Error("Invalid input", input);
    }

    res.json({ output: orderedListOfParentAndChildren });
  } catch (err) {
    console.log("err", err);
    res.json({ invalidInput: false });
  }
});

module.exports.handler = serverless(app);
